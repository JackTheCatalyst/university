import java.util.*;
import java.io.*;

public class Hanoi
{
	private void towers(int n, int src, int dest)
	{
		int tmp;

		if (n <= 0)
			throw new IllegalArgumentException("Cannot operate on 0 disks");
		if (n == 1)
			moveDisk(srs, dest)	// base case: move one disk from src to dest
		else
		{
			tmp = 6 - src - dest;
			towers(n-1, src, tmp);

			moveDisk(srs, dest);
			towers(n-1, tmp, dest);
		}
	}
	private void moveDisk(int src, int dest)
	{
		System.out.println("towers(" + n + ", " + src + ", " + dest + ")");
		System.out.println(       "n=" + n + ", src=" + src + ", tmp=" + tmp);
		System.out.println("Moving top disk from peg " + src + " to peg " + dest);
	}
	public static void main(String[] args)
	{

	}
}