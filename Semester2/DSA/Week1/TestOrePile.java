// Test Harness for OrePile.java
import java.io.*;

public class TestOrePile
{
	public static void main(String args[])
	{
		int iNumTests;
		int iNumPassed;
		Ore validOre;
		OrePile initialOrePile;
		OrePile copyOrePile;
		double weight;
		double grade;

		iNumTests = 0;
		iNumPassed = 0;
		validOre = new Ore(Ore.ORETYPE_IRON, "t");

		// Test with normal conditions

		System.out.println("\n");
		System.out.println("Test Valid Conditions");
		System.out.println("=====================");

		try
		{
			iNumTests++;
			System.out.println("Alt Constructor");
			initialOrePile = new OrePile(validOre, 20.0, 75.0);
			iNumPassed++;
			System.out.println("passed");

			iNumTests++;
			System.out.println("Copy Constructor");
			copyOrePile = new OrePile(initialOrePile);
			iNumPassed++;
			System.out.println("passed");

			iNumTests++;
			System.out.println("getOre");
			if (initialOrePile.getOre() instanceof Ore)
			{
				iNumPassed++;
				System.out.println("passed");
			}
			else
			{
				throw new IllegalStateException("Invalid ore");
			}

			iNumTests++;
			System.out.println("setOre");
			initialOrePile.setOre(validOre);
			iNumPassed++;
			System.out.println("passed");

			iNumTests++;
			System.out.println("getWeight");
			if (initialOrePile.getWeight() == 20.0)
			{
				iNumPassed++;
				System.out.println("passed");
			}
			else 
			{
				throw new IllegalStateException("Invalid weight");
			}

			iNumTests++;
			System.out.println("setWeight");
			initialOrePile.setWeight(20.0);
			iNumPassed++;
			System.out.println("passed");

			iNumTests++;
			System.out.println("getGrade");
			if (initialOrePile.getGrade() == 75.0)
			{
				iNumPassed++;
				System.out.println("passed");
			}
			else
			{
				throw new IllegalStateException("Invalid Grade");
			}

			iNumTests++;
			System.out.println("setGrade");
			initialOrePile.setGrade(75.0);
			iNumPassed++;
			System.out.println("passed");

			iNumTests++;
			System.out.println("calcMetalWt");
			if (initialOrePile.calcMetalWt() == 
				(initialOrePile.getWeight()*initialOrePile.getGrade()/100))
			{
				iNumPassed++;
				System.out.println("passed");
			}
			else
			{
				throw new UnsupportedOperationException("Algorithm Incorrect");
			}
		}

		catch(Exception e)
		{
			System.out.println("FAILED");
		}

		// pass invalid weight
		try
		{
			iNumTests++;
			System.out.println("OrePile(ore, invalidWeight, grade)");
			initialOrePile = new OrePile(validOre, -1, 75.0);
			System.out.println("FAILED");
		}
		catch(Exception e)
		{
			iNumPassed++;
			System.out.println("passed");
		}

		// pass invalid grade
		try
		{
			iNumTests++;
			System.out.println("OrePile(ore, weight, invalidGrade)");
			initialOrePile = new OrePile(validOre, 25.0, -1);
			System.out.println("FAILED");
		}
		catch(Exception e)
		{
			iNumPassed++;
			System.out.println("passed");
		}

		System.out.println("\n");
		System.out.println("Tests: " + iNumTests);
		System.out.println("Passed: " + iNumPassed);
		System.out.println("Failed: " + (iNumTests - iNumPassed));
	}
}