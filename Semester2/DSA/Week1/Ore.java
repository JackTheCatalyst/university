public class Ore
{
	public static final char ORETYPE_IRON = 'I';
	public static final char ORETYPE_NICKEL = 'N';
	
	private char oreType;
	private String units;

	public Ore(char inOreType, String inUnits)
	{
		if (inOreType != Ore.ORETYPE_IRON && inOreType != Ore.ORETYPE_NICKEL)
		{
			throw new IllegalArgumentException("Invalid ore type provided");
		}
		else
		{
			setUnits(inUnits);
			oreType = inOreType;
		}
	}

	public Ore(Ore inOre)
	{
		oreType = inOre.getOreType();
		units 	= inOre.getUnits();	
	}

	public char getOreType()
	{
		return oreType;
	}

	public String getUnits()
	{
		return units;
	}

	public void setUnits(String inUnits)
	{
		if (!inUnits.equals("t") && !inUnits.equals("kg") && !inUnits.equals("g"))
		{
			throw new IllegalArgumentException("Ivalid unit type provided, must be 't', 'kg', 'g'");
		}
		else
		{
			units = inUnits;
		}
	}
}