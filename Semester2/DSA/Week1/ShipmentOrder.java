public class ShipmentOrder
{
	// CLass fields
	private static int nextOrderID 	= 1; // Global counter 
	private double shippedOreWt 	= -1.0;
	private boolean isPending 		= false; 
	
	private int orderID;
	private Ore ore;
	private double unitPrice;
	private String customerName;
	private String destination;
	private double orderedMetalWt;


	// Constructors
	public ShipmentOrder(Ore inOre, double inPrice, String inCustomerName, 
							String inDestination, double inOrderedMetalWt) 
	{
		orderID = nextOrderID;
		nextOrderID++; // Increment the global counter

		ore = inOre;
		setUnitPrice(inPrice);
		setCustomerName(inCustomerName);
		setDestination(inDestination);
		setOrderedMetalWt(inOrderedMetalWt);

		isPending = true;
	}
	public ShipmentOrder(ShipmentOrder inOrder)
	{
		orderID = nextOrderID;
		nextOrderID++;

		ore = inOrder.getOre();
		setUnitPrice(inOrder.getUnitPrice());
		setCustomerName(inOrder.getCustomerName());
		setDestination(inOrder.getDestination());
		setOrderedMetalWt(inOrder.getOrderedMetalWt());
		
		if(inOrder.getShippedOreWt() > 0.0)
		{
			setShippedOreWt(inOrder.getShippedOreWt());
		}
		isPending = inOrder.getIsPending();
	}
	// Getters
	public int getOrderID()
	{
		return orderID;
	}
	public Ore getOre()
	{
		return ore;
	}
	public double getUnitPrice()
	{
		return unitPrice;
	}
	public String getCustomerName()
	{
		return customerName;
	}
	public String getDestination()
	{
		return destination;
	}
	public double getOrderedMetalWt()
	{
		return orderedMetalWt;
	}
	public double getShippedOreWt()
	{
		return shippedOreWt;
	}
	public boolean getIsPending()
	{
		return isPending;
	}
	// Setters
	public void setOre(Ore inOre)
	{
		ore = inOre;
	}
	public void setUnitPrice(double inPrice)
	{
		if (inPrice > 0)
		{
			unitPrice = inPrice;
		}
		else
		{
			throw new IllegalArgumentException("Price must be greater than 0");
		}
	}
	public void setCustomerName(String inCustomerName)
	{
		if (inCustomerName != null)
		{
			customerName = inCustomerName;
		}
		else 
		{
			throw new IllegalArgumentException("Customer name must not be empty");
		}
	}
	public void setDestination(String inDestination)
	{
		if (inDestination != null)	// perhaps use google maps API to validate address
		{
			destination = inDestination;
		}
		else 
		{
			throw new IllegalArgumentException("Destinatinon must not be empty");
		}
	}
	public void setOrderedMetalWt(double inOrderedMetalWt)
	{
		if (inOrderedMetalWt > 0)
		{
			orderedMetalWt = inOrderedMetalWt;
		}
		else
		{
			throw new IllegalArgumentException("Weight must be greater than 0");
		}
	}
	public void setShippedOreWt(double inShippedOreWt)
	{
		if (inShippedOreWt > 0)
		{
			shippedOreWt = inShippedOreWt;
		}
		else
		{
			throw new IllegalArgumentException("Weight must be greater than 0");
		}

		isPending = false; // Indicates order has been shipped
	}
	public boolean equals(ShipmentOrder inOrder)
	{
		boolean isEqual = false;

		if (inOrder instanceof ShipmentOrder)
		{
			if (inOrder.getOrderID() == orderID && ore.equals(inOrder.getOre()) &&
					inOrder.getUnitPrice() == unitPrice && 
					destination.equals(inOrder.getDestination()) &&
					customerName.equals(inOrder.getCustomerName()) && 
					isPending == inOrder.getIsPending() && 
					inOrder.getShippedOreWt() == shippedOreWt &&
					inOrder.getOrderedMetalWt() == orderedMetalWt)
					{
						isEqual = true;
					}
		}
		return isEqual;
	}
	// Imperative Methods
	public double calcAverageGrade()
	{
		double averageGrade;

		if (shippedOreWt > 0.0)
		{
			averageGrade = orderedMetalWt / shippedOreWt;
		}
		else
		{
			averageGrade = 0.0;
			throw new IllegalStateException("Invalid attempt to use an unset variable: shippedOreWt");
		}

		return averageGrade;
	}
	public double calcShipmentValue()
	{
		return orderedMetalWt * unitPrice;
	}
}