// Test Harness for Ore.java
import java.io.*;

public class TestOre
{
	public static void main(String args[])
	{
		int iNumPassed;
		int iNumTests;
		Ore initialOre;
		Ore copyOre;

		iNumPassed = 0;
		iNumTests = 0;

		// Test with normal conditions

		System.out.println("\n");
		System.out.println("Testing Valid Conditions");
		System.out.println("========================");

		try 
		{
			iNumTests++;
			System.out.println("Alt Constructor");
			initialOre = new Ore(Ore.ORETYPE_IRON, "t");
			iNumPassed++;
			System.out.println("passed");

			iNumTests++;
			System.out.println("Copy Constructor");
			copyOre = new Ore(initialOre);
			iNumPassed++;
			System.out.println("passed");

			iNumTests++;
			System.out.println("getOreType");
			if (initialOre.getOreType() == initialOre.ORETYPE_IRON || 
				initialOre.getOreType() == initialOre.ORETYPE_NICKEL)
			{ 
				iNumPassed++;
				System.out.println("passed");
			}
			else
			{
				throw new IllegalStateException("Invalid ORETYPE"); 
			}
			
			iNumTests++;
			System.out.println("getUnits");
			if (initialOre.getUnits().equals("t")  ||
				initialOre.getUnits().equals("kg") ||
				initialOre.getUnits().equals("g"))
			{
				iNumPassed++;
				System.out.println("passed");
			}
			else
			{
				throw new IllegalStateException("Invalid units");
			}

			iNumTests++;
			System.out.println("setUnits");
			initialOre.setUnits("t");
			initialOre.setUnits("kg");
			initialOre.setUnits("g");
			iNumPassed++;
			System.out.println("passed");
		}

		catch(Exception e)
		{
			System.out.println("FAILED");
		}

		System.out.println("\n");
		System.out.println("Testing Exceptional Conditions");
		System.out.println("==============================");

		System.out.println("Alt Constructor");

		// pass in invalid char
		try
		{
			iNumTests++;
			System.out.println("Ore('a', \"t\")");
			initialOre = new Ore('a', "t");
			System.out.println("FAILED");
		}
		catch(Exception e)
		{
			iNumPassed++;
			System.out.println("passed");
		}

		// test units
		try 
		{
			iNumTests++;
			System.out.println("Ore('I', \"a\")");
			initialOre = new Ore('I', "a");
			System.out.println("FAILED");
		} 
		catch(Exception e)
		{
			iNumPassed++;
			System.out.println("passed");
		} 

		System.out.println("\n");
		System.out.println("Tests: " + iNumTests);
		System.out.println("Passed: " + iNumPassed);
		System.out.println("Failed: " + (iNumTests - iNumPassed));
	}
}