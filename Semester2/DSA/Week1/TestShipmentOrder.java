public class TestShipmentOrder
{
	public static void main(String args[])
	{
		int iNumTests = 0;
		int iNumPassed = 0;
		ShipmentOrder initialOrder;
		ShipmentOrder copyOrder;

		Ore validOre = new Ore(Ore.ORETYPE_IRON, "t");
		Double validPrice = 20.0;
		String validName = "John";
		String validDest = "Foo";
		Double validOrdMetWt = 20.0;
		Double validShippedOreWt = 20.0;

		System.out.println("Test Valid Conditions");
		try
		{
			iNumTests++;
			print("Alt Constructor");
			initialOrder = new ShipmentOrder(validOre, validPrice, validName, 
												validDest, validOrdMetWt);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("Copy Constructor");
			copyOrder = new ShipmentOrder(initialOrder);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setOre(validOre)");
			initialOrder.setOre(validOre);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setUnitPrice(validPrice)");
			initialOrder.setUnitPrice(validPrice);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setDestination(validDest)");
			initialOrder.setDestination(validDest);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setOrderedMetalWt(validOrdMetWt)");
			initialOrder.setOrderedMetalWt(validOrdMetWt);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setShippedOreWt(validShippedOreWt)");
			initialOrder.setShippedOreWt(validShippedOreWt);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("equals(copyOrder)");
			initialOrder.equals(copyOrder);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("calcAverageGrade()");
			if (initialOrder.calcAverageGrade() == 
					(
						initialOrder.getOrderedMetalWt() / initialOrder.getShippedOreWt()
					)
				)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalArgumentException("Invalid return from calcAverageGrade()");
			}

			iNumTests++;
			print("calcShipmentValue()");
			if (initialOrder.calcShipmentValue() == 
					(
						initialOrder.getOrderedMetalWt() * initialOrder.getUnitPrice()
					)
				)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalArgumentException("Invalid return from calcShipmentValue()");
			}


			iNumTests++;
			print("getOrderID()");
			if (initialOrder.getOrderID() == 1 && copyOrder.getOrderID() == 2)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid orderID");
			}

			iNumTests++;
			print("getore()");
			if (initialOrder.getOre() instanceof Ore)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid Ore");
			}

			iNumTests++;
			print("getUnitPrice()");
			if (initialOrder.getUnitPrice() == validPrice)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid unitPrice");
			}

			iNumTests++;
			print("getCustomerName()");
			if (initialOrder.getCustomerName().equals(validName))
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid Name");
			}

			iNumTests++;
			print("getDestination()");
			if (initialOrder.getDestination().equals(validDest))
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid Destination");
			}

			iNumTests++;
			print("getOrderedMetalWt()");
			if (initialOrder.getOrderedMetalWt() == validOrdMetWt)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid orderedMetalWt");
			}

			iNumTests++;
			print("getShippedOreWt()");
			if (initialOrder.getShippedOreWt() == validShippedOreWt)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid shippedOreWt");
			}

			iNumTests++;
			print("getIsPending()");
			if (initialOrder.getIsPending() == true || 
					initialOrder.getIsPending() == false)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid isPending");
			}
		}
		catch(Exception e)
		{
			print("FAILED");
		}
	}

	private static void print(String message)
	{
		System.out.println(message);
	}
}