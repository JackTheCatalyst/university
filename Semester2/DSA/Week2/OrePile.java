import java.io.*;
public class OrePile implements Serializable
{
	private Ore ore;
	private double weight;
	private double grade;

	public OrePile()
	{

	}

	public OrePile(Ore inOre, double inWeight, double inGrade)
	{
		setOre(inOre);
		setWeight(inWeight);
		setGrade(inGrade);
	}

	public OrePile(OrePile inOrePile)
	{
		setOre(inOrePile.getOre());
		setWeight(inOrePile.getWeight());
		setGrade(inOrePile.getGrade());
	}

	public Ore getOre()
	{
		return ore;
	}

	public void setOre(Ore inOre)
	{
		if (inOre instanceof Ore)
		{
			ore = inOre;
		}
		else
		{
			throw new IllegalArgumentException("Invalid OreClass provided");
		}
	}

	public double getWeight()
	{
		return weight;
	}

	public void setWeight(double inWeight)
	{
		if (inWeight < 0.0)
		{
			throw new IllegalArgumentException("Invalid weight, can't be negative");
		}
		else
		{
			weight = inWeight;
		}
	}

	public double getGrade()
	{
		return grade;
	}

	public void setGrade(double inGrade)
	{
		if (inGrade < 0.0 || inGrade > 100)
		{
			throw new IllegalArgumentException("Invalid grade");
		}
		else 
		{
			grade = inGrade;
		}
	}

	public String toString()
	{
		String members = ore.toString() + weight + grade;
		return members;
	}

	public double calcMetalWt()
	{
		double metalWt = weight * (grade/100);
		return metalWt;
	}
}