import java.io.*;
public class ShipmentOrder implements Serializable
{
	// CLass fields
	private static int nextOrderID 	= 1; // Global counter 
	private double shippedOreWt 	= -1.0;
	private boolean isPending 		= false; 
	
	private int orderID;
	private Ore ore;
	private double unitPrice;
	private String customerName;
	private String destination;
	private double orderedMetalWt;

	// Constructors
	public ShipmentOrder()
	{
		/* 	
			This empy constructor allows the object to be created 
			in order to load() it.
		*/	
	}
	public ShipmentOrder(Ore inOre, double inPrice, String inCustomerName, 
							String inDestination, double inOrderedMetalWt) 
	{
		orderID = nextOrderID;
		nextOrderID++; // Increment the global counter

		ore = inOre;
		setUnitPrice(inPrice);
		setCustomerName(inCustomerName);
		setDestination(inDestination);
		setOrderedMetalWt(inOrderedMetalWt);

		isPending = true;
	}
	public ShipmentOrder(ShipmentOrder inOrder)
	{
		orderID = nextOrderID;
		nextOrderID++;

		ore = inOrder.getOre();
		setUnitPrice(inOrder.getUnitPrice());
		setCustomerName(inOrder.getCustomerName());
		setDestination(inOrder.getDestination());
		setOrderedMetalWt(inOrder.getOrderedMetalWt());
		
		if(inOrder.getShippedOreWt() > 0.0)
		{
			setShippedOreWt(inOrder.getShippedOreWt());
		}
		isPending = inOrder.getIsPending();
	}
	// Getters
	public int getOrderID()
	{
		return orderID;
	}
	public Ore getOre()
	{
		return ore;
	}
	public double getUnitPrice()
	{
		return unitPrice;
	}
	public String getCustomerName()
	{
		return customerName;
	}
	public String getDestination()
	{
		return destination;
	}
	public double getOrderedMetalWt()
	{
		return orderedMetalWt;
	}
	public double getShippedOreWt()
	{
		return shippedOreWt;
	}
	public boolean getIsPending()
	{
		return isPending;
	}
	// Setters
	public void setOre(Ore inOre)
	{
		if (inOre instanceof Ore)
		{
			ore = inOre;
		}
		else
		{
			throw new IllegalArgumentException("Invalid Ore");
		}
	}
	public void setUnitPrice(double inPrice)
	{
		if (inPrice > 0)
		{
			unitPrice = inPrice;
		}
		else
		{
			throw new IllegalArgumentException("Price must be greater than 0");
		}
	}
	public void setCustomerName(String inCustomerName)
	{
		if (inCustomerName != null && inCustomerName.isEmpty() == false)
		{
			customerName = inCustomerName;
		}
		else 
		{
			throw new IllegalArgumentException("Customer name must not be empty");
		}
	}
	public void setDestination(String inDestination)
	{
		if (inDestination != null && inDestination.isEmpty() == false)
		{
			destination = inDestination;
		}
		else 
		{
			throw new IllegalArgumentException("Destinatinon must not be empty");
		}
	}
	public void setOrderedMetalWt(double inOrderedMetalWt)
	{
		if (inOrderedMetalWt > 0)
		{
			orderedMetalWt = inOrderedMetalWt;
		}
		else
		{
			throw new IllegalArgumentException("Weight must be greater than 0");
		}
	}
	public void setShippedOreWt(double inShippedOreWt)
	{
		if (inShippedOreWt > 0)
		{
			shippedOreWt = inShippedOreWt;
			isPending = false; // Indicates order has been shipped
		}
		else
		{
			throw new IllegalArgumentException("Weight must be greater than 0");
		}
	}
	public boolean equals(ShipmentOrder inOrder)
	{
		boolean isEqual = false;

		if (inOrder instanceof ShipmentOrder)
		{
			if (	ore.equals(inOrder.getOre()) &&
					inOrder.getUnitPrice() == unitPrice && 
					destination.equals(inOrder.getDestination()) &&
					customerName.equals(inOrder.getCustomerName()) && 
					isPending == inOrder.getIsPending() && 
					inOrder.getShippedOreWt() == shippedOreWt &&
					inOrder.getOrderedMetalWt() == orderedMetalWt)
					{
						isEqual = true;
					}
		}
		return isEqual;
	}
	public String toString()
	{
		String members = 	(getOrderID() + 
							ore.toString() + 
							getUnitPrice() + 
							getCustomerName() + 
							getDestination() + 
							getOrderedMetalWt() + 
							getShippedOreWt() +
							nextOrderID +
							isPending);
		return members;
	}
	// Imperative Methods
	public double calcAverageGrade()
	{
		double averageGrade;

		if (shippedOreWt > 0.0)
		{
			averageGrade = orderedMetalWt / shippedOreWt;
		}
		else
		{
			averageGrade = 0.0;
			throw new IllegalStateException("Invalid attempt to use an unset variable: shippedOreWt");
		}

		return averageGrade;
	}
	public double calcShipmentValue()
	{
		return orderedMetalWt * unitPrice;
	}

	// writes object to binary file
	public void save(String fileName)
	{
		FileOutputStream fileStrm = null;
		DataOutputStream dataStrm = null;	

		try 
		{
			fileStrm = new FileOutputStream(fileName);
			dataStrm = new DataOutputStream(fileStrm);

			dataStrm.writeInt(nextOrderID);
			dataStrm.writeDouble(shippedOreWt);
			dataStrm.writeInt(orderID);
			// invokes ore's write() method
			// this lets it handle it's own members
			ore.write(dataStrm); 
			dataStrm.writeDouble(unitPrice);
			dataStrm.writeUTF(customerName);
			dataStrm.writeUTF(destination);
			dataStrm.writeDouble(orderedMetalWt);

			fileStrm.close();
		}
		catch (IOException e)
		{
			if (fileStrm != null)
			{
				try
				{
					fileStrm.close();
				}
				catch (IOException e2)
				{
					System.out.println("Error in file processing: " + e.getMessage());
				}
			}
		}
	}

	public void load(String fileName)
	{
		FileInputStream fileStrm = null;
		DataInputStream dataStrm = null;

		try
		{
			fileStrm = new FileInputStream(fileName);
			dataStrm = new DataInputStream(fileStrm);

			this.nextOrderID = dataStrm.readInt();
			this.setShippedOreWt(dataStrm.readDouble());
			//this.isPending = dataStrm.readBoolean();
			this.orderID = dataStrm.readInt();
			Ore inOre = new Ore(dataStrm.readChar(), dataStrm.readUTF());
			this.setOre(inOre);
			this.setUnitPrice(dataStrm.readDouble());
			this.setCustomerName(dataStrm.readUTF());
			this.setDestination(dataStrm.readUTF());
			this.setOrderedMetalWt(dataStrm.readDouble());

			fileStrm.close();
		}
		catch (IOException e)
		{
			if (fileStrm != null)
			{
				try 
				{
					fileStrm.close();
				}
				catch (IOException e2)
				{
					System.out.println("Error in file processing: " + e.getMessage());
				}
			}
		}
	} 
}