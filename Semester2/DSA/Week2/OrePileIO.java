import java.io.*;
public class OrePileIO
{
	public static void main(String args[])
	{
		Ore ironOreT = new Ore(Ore.ORETYPE_IRON, "t"); 
		OrePile ironOrePile = new OrePile(ironOreT, 20.0, 70.0);

		save(ironOrePile, "savedPile.bin");

		OrePile ironOrePile2 = new OrePile();

		ironOrePile2 = load("savedPile.bin");

		System.out.println(ironOrePile2.toString());
	}

	private static void save(OrePile inOrePile, String fileName)
	{
		FileOutputStream fileStrm;
		ObjectOutputStream objStrm;

		try
		{
			fileStrm = new FileOutputStream(fileName);
			objStrm = new ObjectOutputStream(fileStrm);
			objStrm.writeObject(inOrePile);

			objStrm.close();
		}
		catch (Exception e)
		{
			throw new IllegalArgumentException("Unable to save object to file");
		}
	}

	private static OrePile load(String fileName)
	{
		FileInputStream fileStrm;
		ObjectInputStream objStrm;
		OrePile inOrePile = null;

		try
		{
			fileStrm = new FileInputStream(fileName);
			objStrm = new ObjectInputStream(fileStrm);
			inOrePile = (OrePile)objStrm.readObject();

			objStrm.close();
		}
		catch (ClassNotFoundException e)
		{
			System.out.print("Class OrePile not found " + e.getMessage());
		}
		catch (Exception e)
		{
			throw new IllegalArgumentException("Unable to load object from file");
		}
		return inOrePile;
	}
}