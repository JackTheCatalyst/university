public class TestShipmentOrder
{
	public static void main(String args[])
	{
		int iNumTests = 0;
		int iNumPassed = 0;
		ShipmentOrder initialOrder;
		ShipmentOrder copyOrder;
		ShipmentOrder testOrder;

		Ore validOre = new Ore(Ore.ORETYPE_IRON, "t");
		Double validPrice = 20.0;
		String validName = "John";
		String validDest = "Foo";
		Double validOrdMetWt = 20.0;
		Double validShippedOreWt = 20.0;

		System.out.println("Test Valid Conditions");
		try
		{
			iNumTests++;
			print("Alt Constructor");
			initialOrder = new ShipmentOrder(validOre, validPrice, validName, 
												validDest, validOrdMetWt);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("Copy Constructor");
			copyOrder = new ShipmentOrder(initialOrder);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setOre(validOre)");
			initialOrder.setOre(validOre);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setUnitPrice(validPrice)");
			initialOrder.setUnitPrice(validPrice);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setDestination(validDest)");
			initialOrder.setDestination(validDest);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setOrderedMetalWt(validOrdMetWt)");
			initialOrder.setOrderedMetalWt(validOrdMetWt);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("setShippedOreWt(validShippedOreWt)");
			initialOrder.setShippedOreWt(validShippedOreWt);
			iNumPassed++;
			print("passesd");

			iNumTests++;
			print("equals(copyOrder)");
			copyOrder = new ShipmentOrder(initialOrder);
			if (initialOrder.equals(copyOrder))
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Copied object is not equal");
			}
			iNumTests++;
			print("calcAverageGrade()");
			if (initialOrder.calcAverageGrade() == 
					(
						initialOrder.getOrderedMetalWt() / initialOrder.getShippedOreWt()
					)
				)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalArgumentException("Invalid return from calcAverageGrade()");
			}

			iNumTests++;
			print("calcShipmentValue()");
			if (initialOrder.calcShipmentValue() == 
					(
						initialOrder.getOrderedMetalWt() * initialOrder.getUnitPrice()
					)
				)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalArgumentException("Invalid return from calcShipmentValue()");
			}


			iNumTests++;
			print("getOrderID()");
			if (initialOrder.getOrderID() == 1 && copyOrder.getOrderID() == 3)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid orderID");
			}

			iNumTests++;
			print("getore()");
			if (initialOrder.getOre() instanceof Ore)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid Ore");
			}

			iNumTests++;
			print("getUnitPrice()");
			if (initialOrder.getUnitPrice() == validPrice)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid unitPrice");
			}

			iNumTests++;
			print("getCustomerName()");
			if (initialOrder.getCustomerName().equals(validName))
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid Name");
			}

			iNumTests++;
			print("getDestination()");
			if (initialOrder.getDestination().equals(validDest))
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid Destination");
			}

			iNumTests++;
			print("getOrderedMetalWt()");
			if (initialOrder.getOrderedMetalWt() == validOrdMetWt)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid orderedMetalWt");
			}

			iNumTests++;
			print("getShippedOreWt()");
			if (initialOrder.getShippedOreWt() == validShippedOreWt)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid shippedOreWt");
			}

			iNumTests++;
			print("getIsPending()");
			if (initialOrder.getIsPending() == true || 
					initialOrder.getIsPending() == false)
			{
				iNumPassed++;
				print("passesd");
			}
			else
			{
				throw new IllegalStateException("Invalid isPending");
			}

			iNumTests++;
			print("save() and load()");
			print("This is the data before saving: ");
			print(initialOrder.toString());
			
			initialOrder.save("test.bin");
			testOrder = new ShipmentOrder();
			testOrder.load("test.bin");

			print("This is the data after loading: ");
			print(testOrder.toString());
			iNumPassed++;
			print("passesd");

		}
		catch(Exception e)
		{
			print("FAILED: " + e.getMessage());
		}

		print("Test Invalid Conditions");

		iNumTests++;
		print("ShipmentOrder(validOre, invalidPrice, validName, validDest, validOrdMetWt");
		try
		{
			initialOrder = new ShipmentOrder(validOre, -1.0, validName, validDest, validOrdMetWt);
			print("FAILED");
		}
		catch (Exception e)
		{
			iNumPassed++;
			print("passed");
		}

		iNumTests++;
		print("ShipmentOrder(validOre, validPrice, invalidName, validDest, validOrdMetWt)");
		try
		{
			initialOrder = new ShipmentOrder(validOre, validPrice, "", validDest, validOrdMetWt);
			print("FAILED");
		}
		catch (Exception e)
		{
			iNumPassed++;
			print("passed");
		}

		iNumTests++;
		print("ShipmentOrder(validOre, validPrice, validName, invalidDest, validOrdMetWt)");
		try
		{
			initialOrder = new ShipmentOrder(validOre, validPrice, validName, "", validOrdMetWt);
			print("FAILED");
		}
		catch (Exception e)
		{
			iNumPassed++;
			print("passesd");
		}

		iNumTests++;
		print("ShipmentOrder(validOre, validPrice, validName, validDest, invalidOrdMetWt)");
		try
		{
			initialOrder = new ShipmentOrder(validOre, validPrice, validName, validDest, -1);
			print("FAILED");
		}
		catch (Exception e)
		{
			iNumPassed++;
			print("passed");
		}
	}

	private static void print(String message)
	{
		System.out.println(message);
	}
}