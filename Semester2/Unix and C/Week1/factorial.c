#include <stdio.h>

int factorial(int x);

int main(int argc, char *argv[])
{
	int num;
	scanf("%d", &num);

	num = factorial(num);

	printf("%d\n", num);

	return 0;
}

int factorial(int x)
{
	int ii, n = 1;
	for (ii = 0; ii <= x; ii++)
	{
		n = n*ii;
	}

	return n;
}	