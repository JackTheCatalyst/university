public class Pilot
{
	private String name;
	private int weight;

	public Pilot()
	{
		name = "Unknown";
		weight = 120;
	}
	public Pilot(String inName, int inWeight)
	{
		name = inName;
		weight = inWeight;
	}
	public Pilot(Pilot inPilot)
	{
		name = inPilot.getName();
		weight = inPilot.getWeight();
	}
	public void setName(String inName)
	{
		name = inName;
	}
	public void setWeight(int inWeight)
	{
		weight = validateWeight(inWeight);
	}
	public String getName()
	{
		return name;
	}
	public int getWeight()
	{
		return weight;
	}
	public boolean equals(Pilot inPilot)
	{
		boolean isEqual = false;
		if (name.equals(inPilot.getName()) && weight == inPilot.getWeight())
		{
			isEqual = true;
		}
		return isEqual;
	}
	public String toString()
	{
		String outString = "name: " + name + "weight: " + weight;
		return outString;
	}
	private int validateWeight(int inWeight)
	{
		if ( 0 < inWeight && inWeight < 200)
		{
			weight = inWeight;
		}
		return weight;
	}
}