public class ElectricRacer extends AbstractRacer
{
	private double maxWatts;
	private double ampDraw;
	private double mah;

	public ElectricRacer()
	{
		super();
		maxWatts 	= 400.0;
		ampDraw 	= 40.0;
		mah			= 10000.0;
	}
	public ElectricRacer(double inMaxWatts, double inAmpDraw, double inMah, 
							String inModel, AircraftID inIdentity)
	{
		super(inModel, inIdentity);
		maxWatts 	= validateMaxWatts(inMaxWatts);
		ampDraw 	= validateAmpDraw(inAmpDraw);
		mah 		= validateMah(inMah); 
	}
	public ElectricRacer(ElectricRacer inElectricRacer)
	{
		super(inElectricRacer);
		maxWatts 	= inElectricRacer.getMaxWatts();
		ampDraw 	= inElectricRacer.getAmpDraw();
		mah 		= inElectricRacer.getMah();
	}
	public double getMaxWatts()
	{
		return maxWatts;
	}
	public double getAmpDraw()
	{
		return ampDraw;
	}
	public double getMah()
	{
		return mah;
	}
	public void setMaxWatts(double inMaxWatts)
	{
		maxWatts = validateMaxWatts(inMaxWatts);
	}
	public void setAmpDraw(double inAmpDraw)
	{
		ampDraw = validateAmpDraw(inAmpDraw);
	}
	public void setMah(double inMah)
	{
		mah = validateMah(inMah);
	}
		public boolean equals(ElectricRacer inElectricRacer)
	{
		boolean	isEqual;

		isEqual = (maxWatts == inElectricRacer.getMaxWatts() 
		&& super.equals(inElectricRacer));

		return isEqual;
	}
	public String toString()
	{
		String outString =	" Max Watts " + maxWatts +
							" Amp Draw " + ampDraw +
							" mah " + mah + super.toString();

		return outString;
	}
	private double validateMaxWatts(double inMaxWatts)
	{
		if (inMaxWatts > 400.0 && inMaxWatts < 600.0)
		{
			maxWatts = inMaxWatts;
		}
		return maxWatts;
	}
	private double validateAmpDraw(double inAmpDraw)
	{
		if (inAmpDraw > 20.0 && inAmpDraw < 60.0)
		{
			ampDraw = inAmpDraw;
		}
		return ampDraw;
	}
	private double validateMah(double inMah)
	{
		if (inMah > 10000.0 && inMah < 20000.0)
		{
			mah = inMah;
		}
		return mah;
	}
	private double calcFlightDuration()
	{
		double duration = 60.0 / ((ampDraw * 1000) / mah);
		return duration;
	}
}