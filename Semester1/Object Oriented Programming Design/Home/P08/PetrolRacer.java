public class PetrolRacer extends AbstractRacer
{
	private double fuelConsumption;
	private double fuelLoad;

	public PetrolRacer()
	{
		super();
		fuelConsumption = 10.0;
		fuelLoad 		= 10.0;
	}
	public PetrolRacer(double inFuelConsumption, double inFuelLoad, 
			String inModel, AircraftID inIdentity)
	{
		super(inModel, inIdentity);
		fuelConsumption = inFuelConsumption;
		fuelLoad		= inFuelLoad;
	}
	public PetrolRacer(PetrolRacer inPetrolRacer)
	{
		super(inPetrolRacer);
		fuelConsumption = inPetrolRacer.getFuelConsumption();
		fuelLoad 		= inPetrolRacer.getFuelLoad();
	}
	public double getFuelConsumption()
	{
		return fuelConsumption;
	}
	public double getFuelLoad()
	{
		return fuelLoad;
	}
	public void setFuelConsumption(double inFuelConsumption)
	{
		fuelConsumption = validateFuelConsumption(inFuelConsumption);
	}
	public void setFuelLoad(double inFuelLoad)
	{
		fuelLoad 		= validateFuelLoad(inFuelLoad);
	}
	public boolean equals(PetrolRacer inPetrolRacer)
	{
		boolean isEqual = false;
		isEqual = 	(fuelLoad == inPetrolRacer.getFuelLoad()
						&& super.equals(inPetrolRacer));
		return isEqual;
	}
	public String toString()
	{
		String outString = 	" Fuel Consumption: " + fuelConsumption +
							" Fuel Load " + fuelLoad;
		return outString;
	}
	private double validateFuelConsumption(double inFuelConsumption)
	{
		if (inFuelConsumption >= 8.0 && inFuelConsumption <= 15.0)
		{
			fuelConsumption = inFuelConsumption;
		}
		return fuelConsumption;
	}
	private double validateFuelLoad(double inFuelLoad)
	{
		if (inFuelLoad >= 5.0 && inFuelLoad <= 25.0)
		{
			fuelLoad 		= inFuelLoad;
		}
		return fuelLoad;
	}
	private double calcFlightDuration()
	{
		double duration = (fuelLoad / fuelConsumption) * 60;
		return duration;
	}
}