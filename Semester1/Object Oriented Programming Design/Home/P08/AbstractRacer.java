public abstract class AbstractRacer
{
	private String model;
	private AircraftID identity;

	public AbstractRacer()
	{
		model 	 = new String("No-Model");
		identity = new AircraftID();
	}
	public AbstractRacer(String inModel, AircraftID inIdentity)
	{
		identity 	= inIdentity;
		model 		= new String(inModel);
	}
	public AbstractRacer(AbstractRacer inAbstractRacer)
	{
		identity 	= inAbstractRacer.getIdentity();
		model 		= inAbstractRacer.getModel();
	}
	public void setModel(String inModel)
	{
		model = inModel;
	}
	public void setIdentity(AircraftID inIdentity)
	{
		identity = inIdentity;
	}
	public String getModel()
	{
		return model;
	}
	public AircraftID getIdentity()
	{
		return identity;
	}
	public boolean equals(AbstractRacer inAbstractRacer)
	{
		String inModel = inAbstractRacer.getModel();
		AircraftID inIdentity = inAbstractRacer.getIdentity();

		boolean isEqual = (model.equals(inModel) && identity.equals(inIdentity));
		return isEqual;
	}
	public String toString()
	{
		String outString = 	" Identity: " + identity.toString() + 
							" Model: " + model;
		return outString;
	}
}