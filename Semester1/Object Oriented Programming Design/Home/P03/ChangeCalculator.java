import io.*;

class ChangeCalculator
{
	public static void main(String [] args)
	{
		int changeAsInteger = DoubleToInteger();
		CalcNotesAndCoins(changeAsInteger);
	}
	private static int DoubleToInteger()
	{
		double productCost = ConsoleInput.readDouble("Input cost");
		double paymentMade = ConsoleInput.readDouble("Input amount paid");
		double changeAsDouble = paymentMade - productCost;
		int changeAsInteger = (int)(changeAsDouble * 100);
		return changeAsInteger;
	}
	private static int CalcNotesAndCoins(int changeAsInteger)
	{
		int oneHundredNotes	= changeAsInteger / 10000;
		CheckAndPrint(oneHundredNotes, "Hundred");
		changeAsInteger	= changeAsInteger % 10000;

		int fiftyNotes 		= changeAsInteger / 5000;
		CheckAndPrint(fiftyNotes, "Fifty");
		changeAsInteger	= changeAsInteger % 5000;

		int twentyNotes 	= changeAsInteger / 2000;
		CheckAndPrint(twentyNotes, "Twenty");
		changeAsInteger = changeAsInteger % 2000;

		int tenNotes 		= changeAsInteger / 1000;
		CheckAndPrint(tenNotes, "Ten");
		changeAsInteger = changeAsInteger % 1000;

		int fiveNotes 		= changeAsInteger / 500;
		CheckAndPrint(fiveNotes, "Five");
		changeAsInteger = changeAsInteger % 500;

		int twoDollar 		= changeAsInteger / 200;
		CheckAndPrint(twoDollar, "Two Dollar");
		changeAsInteger = changeAsInteger % 200;

		int oneDollar 		= changeAsInteger / 100;
		CheckAndPrint(oneDollar, "One Dollar");
		changeAsInteger = changeAsInteger % 100;

		int fiftyCoins 		= changeAsInteger / 50;
		CheckAndPrint(fiftyCoins, "Fifty cents");
		changeAsInteger = changeAsInteger % 50;

		int twentyCoins		= changeAsInteger / 20;
		CheckAndPrint(twentyCoins, "Twenty cents");
		changeAsInteger = changeAsInteger % 20;

		int tenCoins		= changeAsInteger / 10;
		CheckAndPrint(tenCoins, "Ten cents");
		changeAsInteger = changeAsInteger % 10;

		int fiveCoins 		= changeAsInteger / 5;
		CheckAndPrint(fiveCoins, "Five cents");
		changeAsInteger = changeAsInteger % 5;

		return changeAsInteger;
	}
	private static void CheckAndPrint(int amount, String type)
	{
		if (amount !=0)
		{
			System.out.println(type + " " + amount);
		}
	}
}
