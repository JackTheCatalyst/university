import io.*;

public class EToTheX
{
	public static void main(String[] args)
	{
		double x = ConsoleInput.readDouble("Please enter x");
		int num = 25;

		double eToTheXNum = eToTheX(x, num);

		System.out.println(eToTheXNum + " " + Math.exp(x));

		System.exit(0);
	}
	private static double eToTheX(double x, int num)
	{
		int numFactorial = 1;
		for (int i = 2; i <= num; i++)
			numFactorial =  numFactorial*i;

			double eToTheXNum = (Math.pow(x, num) / numFactorial);
		for (int i =2; i <=num; i++)
			eToTheXNum += i;
		
		return eToTheXNum;
	}
}