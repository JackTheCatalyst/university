/**
This program takes the dimensions of a wall and calculates 
the amount of paint needed to fully cover it for the input 
number of coats.
*/

import java.util.Scanner;

public class PaintWall
{
	public static void main(String[] args)
	{
		System.out.println("Input length in meters");
		double length = scanner();
		length = validateInput(length);

    	System.out.println("Input width in meters");
		double width = scanner();
		width = validateInput(width);

		//letting user choose how many coats they want
   		System.out.println("Input number of coats");
		double numOfCoats = scanner();
		numOfCoats = validateInput(numOfCoats);

		System.out.println("Input spread of paint in meters sqaure");
		double spread = scanner();
		spread = validateInput(spread);

		double area = inputDimension(length, width, numOfCoats);
		calcNumOfTins(area, spread);
	}
	private static double validateInput(double input)
	{
		while (input < 0.0)
		{
		System.out.println("Please enter a positive number");
		input = scanner();
		}
		return input;
	}
	private static double inputDimension(double length, double width, double numOfCoats)
	{
		double area = length*width*numOfCoats;
		return area;
	}
	//function used to define how many paint tins will be required
	private static void calcNumOfTins(double area, double spread)
	{
    
		double tinsDouble = area/spread;
		//rounding up to nearest whole number to preserve paint needed
		int litresOfPaint = (int)(Math.ceil(tinsDouble*10));
		int tinsInteger   = (int)(Math.ceil(tinsDouble));

		outPut(litresOfPaint, "Litres of paint needed: ");
		outPut(tinsInteger, "Number of 10L paint tins needed: ");
	}
	private static void outPut(int numAnswer, String msg)
	{
		System.out.println(msg + numAnswer);
	}
  	private static double scanner()
  	{
   	 Scanner scan = new Scanner(System.in);
   	 double input = scan.nextDouble();
   	 return input;
  	}
}
