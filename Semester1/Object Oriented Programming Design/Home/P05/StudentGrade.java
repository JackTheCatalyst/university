import java.util.Scanner;

public class StudentGrade
{
	public static void main(String[] args)
	{
		System.out.println("Input Students final mark");
		int mark = scanner();
		mark = validateMark(mark);

		System.out.println("Input number of assessments completed");
		int assessments = scanner();
		assessments = validateAssessments(assessments);

		String 	gradePrefix = "";
		int 	gradeSuffix = 0;
		if (mark >= 50)
		{
			gradePrefix = "" + mark/10;
			gradeSuffix = mark;
		}
		else if (assessments == 5 && mark < 50)
		{
			gradePrefix = "F";
			gradeSuffix = mark;
		}
		else if (assessments < 5 && mark < 50)
		{
			gradePrefix = "DNC";
			gradeSuffix = mark;
		}
		else if (assessments == 0)
		{
			gradePrefix = "DNA";
		}

		System.out.println("Student Grade: " + gradePrefix + "-" + gradeSuffix);
	}
	private static int validateMark(int input)
	{
		while (input < 0 || input > 100)
		{
			System.out.println("Input a number between 0 and 100");
			input = scanner();
		}
		return input;
	}
	private static int validateAssessments(int input)
	{
		while (input < 0 || input > 5)
		{
			System.out.println("Input a number between 0 and 5");
			input = scanner();
		}
		return input;
	}
	private static int scanner()
	{
		Scanner scan = new Scanner(System.in);
		int input = scan.nextInt();
		return input;
	}
}