import io.*;
class poundsToKilos
{
	public static void main(String[] args)
	{
		double pounds = ConsoleInput.readDouble("Input Pounds");
		double kilos = convertPoundsToKilos(pounds);
		outputPoundsAndKilos(pounds, kilos);
		System.exit(0);
	}
	
	private static double convertPoundsToKilos(double pounds)
	{
		double kilos = pounds/2.2046;
		return kilos;
	}
	
	private static void outputPoundsAndKilos(double pounds, double kilos)
	{
		System.out.println("Pounds: " + pounds + " Kilos: " + kilos);
	}
}
