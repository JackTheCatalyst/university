public class AircraftID
{
	private String registration;
	private Pilot pilot;

	public void AircraftID()
	{
		registration 	= "VH-NON";
		pilot 			= new Pilot();
	}
	public void AircraftID(String inRegistration, Pilot inPilot)
	{	
		registration 	= inRegistration;
		pilot 			= inPilot;
	}
	public void AircraftID(AircraftID inAircraftID)
	{
		registration 	= inAircraftID.getRegistration();
		pilot 			= inAircraftID.getPilot();
	}
	public void setRegistration(String inRegistration)
	{
		// registration cannot be validated
		registration = inRegistration;
	}
	public void setPilot(Pilot inPilot)
	{
		pilot = inPilot;
	}
	public String getRegistration()
	{
		return registration;
	}
	public Pilot getPilot()
	{
		return new Pilot(pilot);
	}
	public boolean equals(AircraftID inAircraftID)
	{
		boolean isEquals = false;
		if (registration.equals(inAircraftID.getRegistration()) && pilot == inAircraftID.getPilot())
		{
			isEquals = true;
		}
		return isEquals;
	}
	public String toString()
	{
		String outString = "Registration: " + registration + " " +  pilot.toString();
		return outString;
	}
}