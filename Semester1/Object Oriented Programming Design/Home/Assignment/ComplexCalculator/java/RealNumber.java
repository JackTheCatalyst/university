// FILE:		RealNumber.java
// AUTHOR:		Jack Mordaunt 17754695
// UNIT:		OOPD
// PURPOSE:		This class is used to instantiate instances of a real number 
// 				and defines how it can be mutated with elementary operations
// REFERENCE:	None
// REQUIRES: 	None

public class RealNumber
{
	double realPart;

	public RealNumber()
	{
		realPart = 0.0000;
	}
	public RealNumber(double inRealPart)
	{
		realPart = fourDecimals(inRealPart);
	}
	public RealNumber(RealNumber inRealNumber)
	{
		realPart = inRealNumber.getRealPart();
	}
	public double getRealPart()
	{
		return realPart;
	}
	public void setRealPart(double inRealPart)
	{
		realPart = fourDecimals(inRealPart);
	}
	public boolean equals(RealNumber inRealNumber)
	{
		boolean isEqual = realPart == inRealNumber.getRealPart();
		return isEqual;
	}
	public String toString()
	{
		String outString = "Real Part: " + realPart;
		return outString;
	}
	// this submodule ensures the double is to 4 decimal places
	public static double fourDecimals(double inRealPart)
	{
		double  tempDouble	= inRealPart * 10000;
		int 	tempInt		= (int)tempDouble;
				tempDouble 	= (double)tempInt;
		double	outDouble = tempDouble / 10000;
		return outDouble;
	}
	// These define the operations to mutate the realNumber when parsed another 
	// realNumber object
	public void sum(RealNumber inRealNumber)
	{
		double 	temp = realPart + inRealNumber.getRealPart();
				temp = fourDecimals(temp);

		realPart = temp;
	}
	public void subtract(RealNumber inRealNumber)
	{
		double 	temp = realPart - inRealNumber.getRealPart();
				temp = fourDecimals(temp);

		realPart = temp;
	}
	public void multiply(RealNumber inRealNumber)
	{
		double 	temp = realPart * inRealNumber.getRealPart();
				temp = fourDecimals(temp);

		realPart = temp;
	}
	public void divide(RealNumber inRealNumber)
	{
		double 	temp = realPart / inRealNumber.getRealPart();
				temp = fourDecimals(temp);

		realPart = temp;
	}
}