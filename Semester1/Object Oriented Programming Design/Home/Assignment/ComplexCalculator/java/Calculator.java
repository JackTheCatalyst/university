// FILE:		Calculator.java
// AUTHOR:		Jack Mordaunt 17754695
// UNIT:		OOPD
// PURPOSE:		This program takes real number inputs, performs real and 
// 				complex operations and outputs the result to the console.
// REFERENCE:	None
// REQUIRES: 	RealNumber.java, ComplexNumber.java

public class Calculator
	{
		public static void main(String[] args)
		{
			mainMenu();
			exit();
		}
		private static void exit()
		{
			System.exit(0);
		}
		private static void printMainMenu()
		{
			System.out.println("=================================");
		    System.out.println("| SIMPLY COMPLICATED CALCULATOR |");
		    System.out.println("=================================");
		    System.out.println("|   Options:                    |");
		    System.out.println("|          1. Quit              |");
		    System.out.println("|          2. Real              |");
		    System.out.println("|          3. Complex           |");
		    System.out.println("=================================");
		}
		private static void mainMenu()
		{

			printMainMenu();
			int choice = MenuOption.readInt();
			do
			{
				
				switch(choice)
				{
					case 1 : exit();
					case 2 : realMenu();
					case 3 : complexMenu();
				}
				
			} 
			while(choice < 1 || choice > 3);
		}
		// The sub menu for reals and complex operations is the same so it is 
		// in a submodule
		private static void printSubMenu()
		{
			System.out.println("=================================");
		    System.out.println("| SIMPLY COMPLICATED CALCULATOR |");
		    System.out.println("=================================");
		    System.out.println("|   Options:                    |");
		    System.out.println("|          1. Main Menu         |");
		    System.out.println("|          2. Sum               |");
		    System.out.println("|          3. Subtract          |");
		    System.out.println("|          4. Multiply          |");
		    System.out.println("|          5. Divide            |");
		    System.out.println("=================================");
		}
		private static void realMenu()
		{
			RealNumber realNumberOne = new RealNumber();
			RealNumber realNumberTwo = new RealNumber();

			printSubMenu();
			int choice = MenuOption.readInt();
			do
			{
				switch(choice)
				{
					case 1 : mainMenu();
					case 2 : sum 		(realNumberOne, realNumberTwo);
					case 3 : subtract 	(realNumberOne, realNumberTwo);
					case 4 : multiply 	(realNumberOne, realNumberTwo);
					case 5 : divide 	(realNumberOne, realNumberTwo);
				}
			} 
			while(choice < 1 || choice > 5);
			
			mainMenu();
		}
		private static void complexMenu()
		{
			ComplexNumber complexNumberOne = new ComplexNumber();
			ComplexNumber complexNumberTwo = new ComplexNumber();

			printSubMenu();
			int choice = MenuOption.readInt();
			do
			{
				switch(choice)
				{
					case 1 : mainMenu();
					case 2 : complexSum 	 (complexNumberOne, complexNumberTwo);
					case 3 : complexSubtract (complexNumberOne, complexNumberTwo);
					case 4 : complexMultiply (complexNumberOne, complexNumberTwo);
					case 5 : complexDivide 	 (complexNumberOne, complexNumberTwo);
				}
			}
			while(choice < 1 || choice > 5);
			
			mainMenu();
		}
		// these methods parse RealNumber Objects to the MenuOption class
		// and output the result
		private static void sum(RealNumber realNumberOne, 
									RealNumber realNumberTwo)
		{
			System.out.println("Input a number");
			// calls the MenuOption Object menuItem to get input and parse it 
			// to the RealNumber methods
			realNumberOne = MenuOption.sum(realNumberOne, realNumberTwo);
			// output total
			System.out.println("Total: " + realNumberOne.getRealPart());
			// returns to the real number sub menu
			realMenu();
		}
		private static void subtract(RealNumber realNumberOne, 
										RealNumber realNumberTwo)
		{
			System.out.println("Input a number");

			realNumberOne = MenuOption.subtract(realNumberOne, realNumberTwo);
			
			System.out.println("Total: " + realNumberOne.getRealPart());
			realMenu();
		}
		private static void multiply(RealNumber realNumberOne, 
										RealNumber realNumberTwo)
		{
			System.out.println("Input a number");
			
			realNumberOne = MenuOption.multiply(realNumberOne, realNumberTwo);
			
			System.out.println("Total: " + realNumberOne.getRealPart());
			realMenu();
		}
		private static void divide(RealNumber realNumberOne, 
										RealNumber realNumberTwo)
		{
			System.out.println("Input a number");
			
			realNumberOne = MenuOption.divide(realNumberOne, realNumberTwo);
			
			System.out.println("Total: " + realNumberOne.getRealPart());
			realMenu();
		}
		private static void complexSum(ComplexNumber complexNumberOne, 
											ComplexNumber complexNumberTwo)
		{
			System.out.println("Enter a number for the real part"); 
			System.out.println("Another for the imaginary part"); 

			complexNumberOne = MenuOption.complexSum(complexNumberOne, 
															complexNumberTwo);

			System.out.println("Total: " + complexNumberOne.getRealPart() + 
								" " + complexNumberOne.getImagPart() + "i");
			complexMenu();
		}
		private static void complexSubtract(ComplexNumber complexNumberOne, 
											ComplexNumber complexNumberTwo)
		{
			System.out.println("Enter a number for the real part"); 
			System.out.println("Another for the imaginary part"); 

			complexNumberOne = MenuOption.complexSubtract(complexNumberOne, 
															complexNumberTwo);

			System.out.println("Total: " + complexNumberOne.getRealPart() + 
								" " + complexNumberOne.getImagPart() + "i");
			complexMenu();
		}
		private static void complexMultiply(ComplexNumber complexNumberOne, 
											ComplexNumber complexNumberTwo)
		{
			System.out.println("Enter a number for the real part"); 
			System.out.println("Another for the imaginary part"); 

			complexNumberOne = MenuOption.complexMultiply(complexNumberOne, 
															complexNumberTwo);

			System.out.println("Total: " + complexNumberOne.getRealPart() + 
								" " + complexNumberOne.getImagPart() + "i");
			complexMenu();
		}
		private static void complexDivide(ComplexNumber complexNumberOne, 
											ComplexNumber complexNumberTwo)
		{
			System.out.println("Enter a number for the real part"); 
			System.out.println("Another for the imaginary part"); 

			complexNumberOne = MenuOption.complexDivide(complexNumberOne, 
															complexNumberTwo);

			System.out.println("Total: " + complexNumberOne.getRealPart() + 
								" " + complexNumberOne.getImagPart() + "i");
			complexMenu();
		}

	}