// FILE:		MenuOption.java
// AUTHOR:		Jack Mordaunt 17754695
// UNIT:		OOPD
// PURPOSE:		This static class handles inputs to set values and parse 
//				real and complex objects their operator methods.
// REFERENCE:	None
// REQUIRES: 	RealNumber.java, ComplexNumber.java

import io.*;
public class MenuOption
{
	public MenuOption()
	{

	}
	public MenuOption(MenuOption inMenuOption)
	{

	}
	public boolean equals(MenuOption inMenuOption)
	{
		boolean isEqual = true; 
		return isEqual;
	}
	public String toString()
	{
		String outString = "Static Methods: sum() subtract() multiply() divide()" 
							+ " complexSum() complexSubtract() complexMultiply()" 
							+ " complexDivide()";
		return outString;
	}
	public static int readInt()
	{
		int inInt = ConsoleInput.readInt();
		return inInt;
	}
	// Adds together 2 RealNumber Objects
	public static RealNumber sum(RealNumber realNumberOne, 
									RealNumber realNumberTwo)
	{
		// set  value for first number object
		double input;
		input = ConsoleInput.readDouble();
		realNumberOne.setRealPart(input);

		// set value for second number object
		input = ConsoleInput.readDouble();
		realNumberTwo.setRealPart(input);

		// parse the second object to the first ones "sum" method
		realNumberOne.sum(realNumberTwo);
		
		return realNumberOne;
	}
	public static RealNumber subtract(RealNumber realNumberOne, 
									RealNumber realNumberTwo)
	{
		double input;

		input = ConsoleInput.readDouble();
		realNumberOne.setRealPart(input);

		input = ConsoleInput.readDouble();
		realNumberTwo.setRealPart(input);

		realNumberOne.subtract(realNumberTwo);
		
		return realNumberOne;
	}
	public static RealNumber multiply(RealNumber realNumberOne, 
									RealNumber realNumberTwo)
	{
		double input;

		input = ConsoleInput.readDouble();
		realNumberOne.setRealPart(input);

		input = ConsoleInput.readDouble();
		realNumberTwo.setRealPart(input);

		realNumberOne.multiply(realNumberTwo);
		
		return realNumberOne;
	}
	public static RealNumber divide(RealNumber realNumberOne, 
									RealNumber realNumberTwo)
	{
		double input;

		input = ConsoleInput.readDouble();
		realNumberOne.setRealPart(input);

		input = ConsoleInput.readDouble();
		realNumberTwo.setRealPart(input);

		realNumberOne.divide(realNumberTwo);
		
		return realNumberOne;
	}
	public static ComplexNumber complexSum(ComplexNumber complexNumberOne, 
											ComplexNumber complexNumberTwo)
	{
		double inRealPart, inImagPart;

		// set initial real value for first complex number	
		inRealPart = ConsoleInput.readDouble();
		complexNumberOne.setRealPart(inRealPart);

		// set initial imaginary value for first complex number	
		inImagPart = ConsoleInput.readDouble();
		complexNumberOne.setImagPart(inImagPart);

		// set initial real value for second complex number
		inRealPart = ConsoleInput.readDouble();
		complexNumberTwo.setRealPart(inRealPart);

		// set initial imaginary value for second complex number
		inImagPart = ConsoleInput.readDouble();
		complexNumberTwo.setImagPart(inImagPart);

		// parses the second complex number to the first ones "sum" method
		complexNumberOne.sum(complexNumberTwo);

		return complexNumberOne;
	}
	public static ComplexNumber complexSubtract(ComplexNumber complexNumberOne, 
											ComplexNumber complexNumberTwo)
	{
		double inRealPart, inImagPart;

		inRealPart = ConsoleInput.readDouble();
		complexNumberOne.setRealPart(inRealPart);

		inImagPart = ConsoleInput.readDouble();
		complexNumberOne.setImagPart(inImagPart);

		inRealPart = ConsoleInput.readDouble();
		complexNumberTwo.setRealPart(inRealPart);

		inImagPart = ConsoleInput.readDouble();
		complexNumberTwo.setImagPart(inImagPart);

		complexNumberOne.subtract(complexNumberTwo);
		
		return complexNumberOne;
	}
	public static ComplexNumber complexMultiply(ComplexNumber complexNumberOne, 
											ComplexNumber complexNumberTwo)
	{
		double inRealPart, inImagPart;

		inRealPart = ConsoleInput.readDouble();
		complexNumberOne.setRealPart(inRealPart);

		inImagPart = ConsoleInput.readDouble();
		complexNumberOne.setImagPart(inImagPart);

		inRealPart = ConsoleInput.readDouble();
		complexNumberTwo.setRealPart(inRealPart);

		inImagPart = ConsoleInput.readDouble();
		complexNumberTwo.setImagPart(inImagPart);
		
		complexNumberOne.multiply(complexNumberTwo);
		
		return complexNumberOne;
	}
	public static ComplexNumber complexDivide(ComplexNumber complexNumberOne, 
											ComplexNumber complexNumberTwo)
	{
		double inRealPart, inImagPart;

		inRealPart = ConsoleInput.readDouble();
		complexNumberOne.setRealPart(inRealPart);

		inImagPart = ConsoleInput.readDouble();
		complexNumberOne.setImagPart(inImagPart);

		inRealPart = ConsoleInput.readDouble();
		complexNumberTwo.setRealPart(inRealPart);

		inImagPart = ConsoleInput.readDouble();
		complexNumberTwo.setImagPart(inImagPart);
		
		complexNumberOne.divide(complexNumberTwo);
		
		return complexNumberOne;
	}
}