// FILE:		ComplexNumber.java
// AUTHOR:		Jack Mordaunt 17754695
// UNIT:		OOPD
// PURPOSE:		This class is used to instantiate isntances of a complex 
//				number and defines how it can be mutated with complex number 
//				operations.
// REFERENCE:	None
// REQUIRES: 	RealNumber.java

public class ComplexNumber extends RealNumber
{
	double imagPart;

	public ComplexNumber()
	{
		super();
		imagPart = 0.0000;
	}
	public ComplexNumber(double inRealPart, double inImagPart)
	{
		super(inRealPart);
		imagPart = super.fourDecimals(inImagPart);
	}
	public ComplexNumber(ComplexNumber inComplexNumber)
	{
		super(inComplexNumber);
		imagPart = inComplexNumber.getImagPart();
	}
	public double getImagPart()
	{
		return imagPart;
	}
	public void setImagPart(double inImagPart)
	{
		imagPart = super.fourDecimals(inImagPart);
	}
	public boolean equals(ComplexNumber inComplexNumber)
	{
		boolean isEqual = false;
		if(imagPart == inComplexNumber.getImagPart()
							&& super.getRealPart() == inComplexNumber.getRealPart() );
		{
			isEqual = true;
		}
		return isEqual;
	}
	public String toString()
	{
		String outString = super.toString() + "Imaginary Part: " + imagPart;
		return outString;
	}
	// These define the operations to mutate the ComplexNumber when parsed 
	// another ComplexNumber object 
	// The algorithms break the calculation down into steps and store those
	// steps in the temporary variables in an attempt to make it more readable
	public void sum(ComplexNumber inComplexNumber)
	{
		double tempReal = super.getRealPart() + inComplexNumber.getRealPart();
		double tempImag = imagPart 			+ inComplexNumber.getImagPart();

		super.setRealPart(tempReal);
		imagPart = tempImag;
	}
	public void subtract(ComplexNumber inComplexNumber)
	{
		double tempReal = super.getRealPart() - inComplexNumber.getRealPart();
		double tempImag = imagPart 			- inComplexNumber.getImagPart();

		super.setRealPart(tempReal);
		imagPart = tempImag;
	}
	public void multiply(ComplexNumber inComplexNumber)
	{
		double tempRealToReal = super.getRealPart() 	* inComplexNumber.getRealPart();
		double tempImagToReal = imagPart 			* inComplexNumber.getImagPart();

		double tempReal = tempRealToReal - tempImagToReal;

		double tempRealToImag = imagPart 		  * inComplexNumber.getRealPart();
		double tempImagToImag = super.getRealPart() * inComplexNumber.getImagPart();

		double tempImag = tempRealToImag + tempImagToImag;

		super.setRealPart(tempReal);
		imagPart = tempImag;
	}
	public void divide(ComplexNumber inComplexNumber)
	{
		double realNumerator = super.getRealPart() * inComplexNumber.getRealPart()
								+ imagPart * inComplexNumber.getImagPart();

		double realDenominator = Math.pow(super.getRealPart(), 2) + 
									Math.pow(inComplexNumber.getImagPart(), 2);

		double imagNumerator = imagPart * inComplexNumber.getRealPart() - 
									super.getRealPart() * inComplexNumber.getImagPart();

		double imagDenominator = Math.pow(super.getRealPart(), 2) + 
									Math.pow(inComplexNumber.getImagPart(), 2);

		double tempReal = realNumerator / realDenominator;
		double tempImag = imagNumerator / imagDenominator;
		
		super.setRealPart(tempReal);
		imagPart = tempImag;
	}
}