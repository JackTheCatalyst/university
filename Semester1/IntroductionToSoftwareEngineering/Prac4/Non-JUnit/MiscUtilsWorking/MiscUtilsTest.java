public class MiscUtilsTest
{
	public static void main(String[] args)
	{
		testMax();
		testCalcGrade();
		testRoomArea();
	}
	public static void testMax()
	{
		int actual;

		actual = MiscUtils.max(2, 1);
		assert  2 == actual; 
		
		actual = MiscUtils.max(1, 1);
		assert  1 == actual;

		actual = MiscUtils.max(1, 2);
		assert 2 == actual;
	}
	public static void testCalcGrade()
	{
		String actualString;

		actualString = MiscUtils.calcGrade(50);
		assert "5".equals(actualString);

		actualString = MiscUtils.calcGrade(59);
		assert "5".equals(actualString);

		actualString = MiscUtils.calcGrade(60);
		assert "6".equals(actualString);

		actualString = MiscUtils.calcGrade(69);
		assert "6".equals(actualString);
	
		actualString = MiscUtils.calcGrade(70);
		assert "7".equals(actualString);

		actualString = MiscUtils.calcGrade(79);
		assert "7".equals(actualString);

		actualString = MiscUtils.calcGrade(80);
		assert "8".equals(actualString);

		actualString = MiscUtils.calcGrade(89);
		assert "8".equals(actualString);

		actualString = MiscUtils.calcGrade(90);
		assert "9".equals(actualString);

		actualString = MiscUtils.calcGrade(99);
		assert "9".equals(actualString);

		actualString = MiscUtils.calcGrade(100);
		assert "1".equals(actualString);

		actualString = MiscUtils.calcGrade(-1);
		assert "".equals(actualString);

		actualString = MiscUtils.calcGrade(0);
		assert "F".equals(actualString);

		actualString = MiscUtils.calcGrade(49);
		assert "F".equals(actualString);

		actualString = MiscUtils.calcGrade(101);
		assert "".equals(actualString);
		 
	}
	public static void testRoomArea()
	{
		int actual;

		actual = MiscUtils.roomArea(5,5);
		assert 25 == actual;
	}
}
