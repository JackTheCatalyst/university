/*
This testmodule uses JUnit to test the corresponding java classes
*/

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert;
// These imports are require for every JUnit class

@RunWith(JUnit4.class)
public class MiscUtilsTest
{
	@Test
	public void testMax()
	{
		int actual;
		
		actual = MiscUtils.max(2, 1);
		assertEquals(2, actual); 
		
		actual = MiscUtils.max(1, 1);
		assertEquals(1, actual);

		actual = MiscUtils.max(1, 2);
		assertEquals(2, actual);
		*/
	}
	@Test
	public void testCalcGrade()
	{
		String actualString;

		actualString = MiscUtils.calcGrade(50);
		assertEquals("5", actualString);

		actualString = MiscUtils.calcGrade(59);
		assertEquals("5", actualString);

		actualString = MiscUtils.calcGrade(60);
		assertEquals("6", actualString);

		actualString = MiscUtils.calcGrade(69);
		assertEquals("6", actualString);
	
		actualString = MiscUtils.calcGrade(70);
		assertEquals("7", actualString);

		actualString = MiscUtils.calcGrade(79);
		assertEquals("7", actualString);

		actualString = MiscUtils.calcGrade(80);
		assertEquals("8", actualString);

		actualString = MiscUtils.calcGrade(89);
		assertEquals("8", actualString);

		actualString = MiscUtils.calcGrade(90);
		assertEquals("9", actualString);

		actualString = MiscUtils.calcGrade(99);
		assertEquals("9", actualString);

		actualString = MiscUtils.calcGrade(100);
		assertEquals("1", actualString);

		actualString = MiscUtils.calcGrade(-1);
		assertEquals("", actualString);

		actualString = MiscUtils.calcGrade(0);
		assertEquals("F", actualString);

		actualString = MiscUtils.calcGrade(49);
		assertEquals("F", actualString);

		actualString = MiscUtils.calcGrade(101);
		assertEquals("", actualString);
		 
	}
	@Test
	public void testRoomArea()
	{
		int actual;

		actual = MiscUtils.roomArea(5,5);
		assertEquals(25, actual);
	}
}
