import io.*;

/**
 * This is a program for performing one of a list of simple statistical 
 * operations on a list of numbers.
 *
 * OOPD doesn't cover "arrays" or "lists", so you may not know how to store and
 * access a list of numbers in memory. We'll use a non-standard class/datatype 
 * called DoubleList to short-circuit this problem.
 *
 * Variables of type "DoubleList" can store (or, technically, reference) a 
 * whole list of real numbers. The list can be any length, including zero. The
 * only two list-related operations we need are:
 *
 * 1) DoubleList.read(...), to input a list of real numbers from the user;
 *
 * 2) list.get(i), which retrieves the i'th number from a list, starting from
 *    zero, assuming that the 'list' variable is of type DoubleList. (For 
 *    example, if the list contains 2.5, 5.0 and 1.0, and if i=0, then 
 *    list.get(i) will return 2.5.)
 *
 * (This class is part of SE110 Practical Worksheet 7 (Modularity).)
 */
public class Statistics
{
    public static void main(String[] args)
    {
        String operation;
        int listLength;
        DoubleList list;
        double result;
        
        // Input a list of numbers, and the operation to perform.
        listLength = ConsoleInput.readInt("Enter length of list");
        list = DoubleList.read(listLength, "Enter list of real numbers (separated by spaces)");
        operation = ConsoleInput.readWord("Select a calculation to perform");
        
        // Determine which operation was chosen, and perform it.
        if(operation.equals("min"))
        {
            result = max(listLength, list);
        }
        else if(operation.equals("max"))
        {
            result = max(listLength, list);
        }
        else if(operation.equals("sum"))
        {   
            result = sum(listLength, list);
        }
        else if(operation.equals("mean"))
        {
            result = mean(listLength, list);
        }
        else if(operation.equals("variance"))
        { 
            result = variance(listLength, list);
        }
        else if(operation.equals("stddev"))
        {
            result = stddev(listLength, list);
        }
        else if (operation.equals("product"))
        {
            result = product(listLength, list);
        }
        else if (operation.equals("pow"))
        {
            result = pow(listLength, list);
        }
        else if(operation.equals("geommean"))
        {
            result = geomean(listLength, list);
        }
        else if (operation.equals("log"))
        {
            result = log(listLength, list);
        }
        else
        {
            System.out.println("Unrecognised operation \"" + operation + "\".");
            result = 0.0;
        }
        System.out.println("Result = " + result);
    }
    
    /**
     * Calculates the sum of the numbers in the sumList variable.
     * @return The sum.
     */
    public static double sum(int listLength, DoubleList list)
    {
        double result = 0.0;
        for(int i = 0; i < listLength; i++)
        {
            result += list.get(i);
        }
        return result;
    }
    
    
    /**
     * Calculates the mean (average) of the numbers in the sumList variable.
     * @return The mean.
     */
    public static double mean(int listLength, DoubleList list)
    {
        double sum = 0.0;
        for(int i = 0; i < listLength; i++)
        {
            sum += list.get(i);
        }
        return sum / (double)listLength;
    }
    
    /**
     * Calculates the variance of a list of numbers. Stores the result in the 
     * varianceResult variable.
     * @param length The list length.
     * @param list The list of numbers.
     */
    public static double variance(int listLength, DoubleList list)
    {
        double average;
        double difference;
        double sumSquares = 0.0;
        
        average = mean(listLength, list);
        
        for(int i = 0; i < listLength; i++)
        {
            difference = list.get(i) - average;
            sumSquares += difference * difference;
        }
        
        double varianceResult = sumSquares / ((int)listLength - 1);
        return varianceResult;
    }
    
    /**
     * Calculates the standard deviation of a list of numbers. 
     * @param length The list length.
     * @param list The list of numbers.
     * @return The standard deviation.
     */
    public static double stddev(int listLength, DoubleList list)
    {
        return Math.sqrt(variance(listLength, list));
    }
    
    /** 
     * Determines either the lowest or highest of a list of numbers.
     * @param length The list length.
     * @param list The list of numbers.
     * @param max Whether to determine the maximum or minimum. If max is true,
     * the maximum is found; otherwise, the minimum is found.
     * @return The maximum or minimum.
     */

    public static double max(int listLength, DoubleList list)
    {
            // Find the lowest value in the list.
            double result = list.get(0);
            for(int i = 1; i < listLength; i++)
            {
                double  element = list.get(i);
                if(result < element) 
                {
                    // If the next element is lower than the minimum so far, 
                    // update the minimum.
                    result = element;
                }
            }
            return result;
    }
    public static double min(int listLength, DoubleList list)
    {
        // Find the highest value in the list.
            double result = list.get(0);
            for(int i = 1; i < listLength; i++)
            {
                double  element = list.get(i);
                if(result > element) 
                {
                    // If the next element is higher than the maximum so far, 
                    // update the maximum.
                    result = element;
                }
            }
            return result;
    }
    /**
     * Calculates the product of a list of numbers, and optionally performs an
     * additional operation.
     * @param op An extra operation to perform. If op is 1, we calculate the 
     * "geometric mean". If op is 2, we find the log of the product. Otherwise,
     * we just return the raw product.
     * @param length The list length.
     * @param list The list of numbers.
     * @return The product, or log of the product, or geometric mean.
     */
    public static double product(int listLength, DoubleList list)
    {
        double result = 1.0;
        for(int i = 0; i < listLength; i++)
        {
            result *= list.get(i);
        }
        return result;
    }
    private static double pow(int listLength, DoubleList list)
    {
        double result =  Math.pow(product(listLength, list), 1.0 / (double)listLength);
        return result;
    }
    private static double log(int listLength, DoubleList list)
    {
        double result = Math.log(product(listLength, list));
        return result;
    }
    private static double geomean(int listLength, DoubleList list)
    {
        double result;
        double product;

        product = product(listLength, list);
        result = Math.pow(product, 1.0 / (double)listLength);

        return result;
    }

}